# Set title of the window
set-option -g set-titles on
set-option -g set-titles-string 'lu-tmux'

# Windows and panes numbered from 0
set -g base-index 0
set-window-option -g pane-base-index 0
# renumber windows when a window is closed
set -g renumber-windows on

# increase scrollback lines
set -g history-limit 50000

# Notifications
setw -g monitor-activity on
set -g visual-activity off

# Status bar
set -g status-interval 1
set -g status-left-length 256
set -g status-right-length 256
set-option -g status on
set-option -g status-interval 2
set-option -g status-justify "left"
set-option -g status-left-length 60
set-option -g status-right-length 90
set-option -g status-position bottom

# Improve colors
set -g default-terminal 'screen-256color'

# Change default shell to ZSH:
set-option -g default-shell /usr/bin/zsh

# Enable copy-vi-mode
set-window-option -g mode-keys vi

# Do not set aggressive-resize on your own, it collides with theme
# setw -g aggressive-resize

# Start numbering windows from 1
set -g base-index 1
setw -g pane-base-index 1

# Key bindings
source ~/.tmux/includes/.tmux-keybindings
# Load plugins
source ~/.tmux/includes/.tmux-plugins

# Auto-restore
set -g @continuum-restore 'on'

