" Use Vim settings, rather then Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible
filetype off

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" Buffer unite
Plugin 'https://github.com/Shougo/denite.nvim'
" Project handling
Plugin 'https://github.com/dunstontc/projectile.nvim'

" --------------------- PROGRAMMING ------------------------ "
" C/C++ plugins
Plugin 'vhdirk/vim-cmake'
Plugin 'lyuts/vim-rtags'
Plugin 'neoclide/coc.nvim'
Plugin 'w0rp/ale'
Plugin 'https://github.com/rhysd/vim-clang-format.git'

" Gdb for neovim
Plugin 'sakhnik/nvim-gdb', { 'do': ':!./install.sh \| UpdateRemotePlugins' }

" Highlight the shader code
Plugin 'tikhomirov/vim-glsl'

" Spell check for code camelCase and snake_case
Plugin 'shinglyu/vim-codespell'

" Git support
Plugin 'https://github.com/tpope/vim-fugitive.git'
" ---------------------------------------------------------- "

" --------- TEXT AND NAVIGATION ---------------------------- "
" Fuzzy search
Plugin 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plugin 'junegunn/fzf.vim'
" Use fzf for searching coc lists
Plugin 'antoinemadec/coc-fzf'

" Easy motion
Plugin 'easymotion/vim-easymotion'

" Make colors for bookmarks
Plugin 'MattesGroeger/vim-bookmarks'

" Replace { with [ using cs{[ and stuff like this
Plugin 'https://github.com/tpope/vim-surround'
" ---------------------------------------------------------- "

" -------------------- FILESYSTEM -------------------------- "
" Useless but maybe screen sharing
Plugin 'https://github.com/scrooloose/nerdtree.git'
" ---------------------------------------------------------- "

" --------------------- MARKUPS ---------------------------- "
" Markdown
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
Plugin 'JamshedVesuna/vim-markdown-preview'
" ---------------------------------------------------------- "

" ---------------- VISUAL EDITING -------------------------- "
" Visual editing of CSV files (spreadsheet like)
Plugin 'chrisbra/csv.vim'

" Hex editor
Plugin 'https://github.com/fidian/hexmode.git'

" Drawing diagrams in vim
Plugin 'gyim/vim-boxdraw'
" ---------------------------------------------------------- "

" --------------------- COLORTHEMES ------------------------ "
" Airline and onehalf theme are just fine
Plugin 'sonph/onehalf', {'rtp': 'vim/'}
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
" ---------------------------------------------------------- "

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" Loads my vundle configuration:

"source ~/.vim/bundle/Vundle.vim/myconf.vim

" TODO: this may not be in the correct place. It is intended to allow overriding <Leader>.
" source ~/.vimrc.before if it exists.
if filereadable(expand("~/.vimrc.before"))
    source ~/.vimrc.before
endif

" ================ General Config ====================

set number                      "Line numbers are good
set backspace=indent,eol,start  "Allow backspace in insert mode
set history=1000                "Store lots of :cmdline history
set showcmd                     "Show incomplete cmds down the bottom
set showmode                    "Show current mode down the bottom
set gcr=a:blinkon0              "Disable cursor blink
set visualbell                  "No sounds
set autoread                    "Reload files changed outside vim

" This makes vim act like all other editors, buffers can
" exist in the background without being in a window.
" http://items.sjbach.com/319/configuring-vim-right
set hidden

"turn on syntax highlighting
syntax on

" Change leader to a comma because the backslash is too far away
" That means all \x commands turn into ,x
" The mapleader has to be set before vundle starts loading all
" the plugins.
nnoremap <space> <nop>
let mapleader=" "

let maplocalleader="-"

" =============== Vundle Initialization ===============
" This loads all the plugins specified in ~/.vim/vundle.vim
" Use Vundle plugin to manage all other plugins
if filereadable(expand("~/.vim/vundles.vim"))
    source ~/.vim/vundles.vim
endif

" ================ Search Settings  =================

set incsearch        "Find the next match as we type the search
set hlsearch         "Hilight searches by default
set viminfo='100,f1  "Save up to 100 marks, enable capital marks

" ================ Turn Off Swap Files ==============

set noswapfile
set nobackup
set nowb

" ================ Persistent Undo ==================
" Keep undo history across sessions, by storing in file.
" Only works all the time.

silent !mkdir ~/.vim/backups > /dev/null 2>&1
set undodir=~/.vim/backups
set undofile

" ================ Indentation ======================
set autoindent
set smartindent
set smarttab
set shiftwidth=4
set softtabstop=2
set tabstop=2
set expandtab
" Minimum width of line number column
set numberwidth=5
" Set sign column always visible to stop jumping when 0/1 signs toggle
set signcolumn=yes

filetype plugin on
filetype indent on

" Display tabs and trailing spaces visually
set list listchars=tab:\ \ ,trail:·

set nowrap       "Don't wrap lines
set linebreak    "Wrap lines at convenient points

" ================ Folds ============================

set foldmethod=indent   "fold based on indent
set foldnestmax=3       "deepest fold is 3 levels
set nofoldenable        "dont fold by default

" ================ Completion =======================

set wildmode=list:longest
set wildmenu                "enable ctrl-n and ctrl-p to scroll thru matches
set wildignore=*.o,*.obj,*~ "stuff to ignore when tab completing
set wildignore+=*vim/backups*
set wildignore+=*sass-cache*
set wildignore+=*DS_Store*
set wildignore+=vendor/rails/**
set wildignore+=vendor/cache/**
set wildignore+=*.gem
set wildignore+=log/**
set wildignore+=tmp/**
set wildignore+=*.png,*.jpg,*.gif

"

" ================ Scrolling ========================

set scrolloff=8         "Start scrolling when we're 8 lines away from margins
set sidescrolloff=15
set sidescroll=1

" =============== buffers - shortcuts ========
if has('nvim')
    " nothing
else
    exec "set <M-n>=\en"
    exec "set <M-t>=\et"
endif

" navigate between previous and next buffer
nnoremap <M-n> :bn<CR>
nnoremap <M-t> :bp<CR>
nnoremap <D-n> :bn<CR>
nnoremap <D-t> :bp<CR>

" set path from where vim was started
set path=$PWD/**


" highlight Comment cterm=italic
au WinLeave * set nocursorline nocursorcolumn
au WinEnter * set cursorline cursorcolumn
set cursorline cursorcolumn

" map escape sequences to leave insert mode
:inoremap jj <Esc>

" let us get crazy and swap l and s in dvorak layout
" h and s match perfectly to go left and right
" j and k are fine as well
" I don't use s that much so it would be fine for learning time
" to not have it at all
:nnoremap l <nop>
:nnoremap s l
:vnoremap l <nop>
:vnoremap s l

" ccls for vim-lsp
if executable('ccls')
    au User lsp_setup call lsp#register_server({
                \ 'name': 'ccls',
                \ 'cmd': {server_info->['ccls']},
                \ 'root_uri': {server_info->lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_file_directory(lsp#utils#get_buffer_path(), 'compile_commands.json'))},
                \ 'initialization_options': { 'cacheDirectory': '.ccls-cache' },
                \ 'whitelist': ['c', 'cpp', 'objc', 'objcpp', 'cc'],
                \ })
endif

"""""""""""""""""""""""""""""""""""""""""""""""
" FUZZY SEARCH
" search the list of files in current directory
nnoremap <Leader>ff :Files<CR>
nnoremap <M-f> :Files<CR>

" search list of open buffers
nnoremap <Leader>bb :Buffers<CR>
nnoremap <M-b> :Buffers<CR>

" search history files
nnoremap <Leader>hh :History<CR>
nnoremap <M-h> :History<CR>

" search current buffer fuzzy-ly
nnoremap <Leader>// :BLines<CR>
nnoremap <M-/> :BLines<CR>

" search previous commands
nnoremap <Leader>cc :History:<CR>
nnoremap <M-c> :History:<CR>

" search previous searches /
" it does not work with fuzzy searches though
nnoremap <Leader>ss :History/<CR>
nnoremap <M-s> :History/<CR>

" Search Open files (buffers)
nnoremap <Leader>oo :Lines<CR>
nnoremap <M-o> :Lines<CR>

" Search Word under cursor (cursor anywhere in word)
nnoremap <Leader>sw yiw:Rg <C-r>"<CR>

" Search Text, with rg, press enter after
nnoremap <M-r> :Rg<Space>

" Remap keys for gotos
" remember the gf gg
nnoremap <silent> gd <Plug>(coc-definition)
nnoremap <silent> gt <Plug>(coc-type-definition)
nnoremap <silent> gi <Plug>(coc-implementation)
nnoremap <silent> gr <Plug>(coc-references)
" display outline
nnoremap <silent> go :CocFzfList outline<cr>
nnoremap <silent> gs :CocList symbols<cr>
nnoremap <leader>rn <Plug>(coc-rename)

" Copy Window actions to work with leader sequence
nnoremap <Leader>ww <C-w><C-w>
nnoremap <Leader>wv <C-w><C-v>
nnoremap <Leader>ws <C-w><C-s>
nnoremap <Leader>wq <C-w>q

" Easy motion to navigate on the screen
" Jump to line
nnoremap <Leader>jl <Plug>(easymotion-overwin-line)
" Jump to word
nnoremap <Leader>jw <Plug>(easymotion-overwin-w)

" terminal shortcuts
tnoremap jj <C-\><C-n>

" edit and source .vimrc
nnoremap <leader>ev :vsplit $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>

let g:fzf_preview_window = ''
let g:fzf_layout = { 'window': { 'width': 0.8, 'height': 0.6 } }

" use grip
let vim_markdown_preview_github=1

let g:projectile#directory_command = 'cd '

" Colors, themes and stuff

" set default colorscheme to onehalfdark
colorscheme onehalflight
"let g:airline_theme='onehalfdark'
let g:airline_theme='onehalflight'

" column and row highlight overriding default scheme
highlight CursorColumn ctermbg=253
highlight CursorLine ctermbg=253
highlight CursorLineNr ctermbg=DarkRed
highlight CursorLineNr cterm=bold
highlight Comment ctermfg=Grey

" show delimiters on code size for 80, 100 and 120th column
set colorcolumn=80,100,120
highlight ColorColumn ctermbg=LightRed

" colors of tabs for buftabline
:hi TabLineFill ctermfg=None ctermbg=None
:hi TabLine ctermfg=Blue ctermbg=None
:hi TabLineSel ctermfg=Red ctermbg=Yellow

" Bookmarks colors
highlight BookmarkSign ctermbg=NONE ctermfg=160
highlight BookmarkLine ctermbg=LightBlue ctermfg=NONE
" let g:bookmark_sign = '♥'
let g:bookmark_highlight_lines = 1
" enable enhanced tabline (buftabline is disabled)
let g:airline#extensions#tabline#enabled = 1
" only display root of the file (no directory, no extension - to save space
" you can always see the rest with :Buffers :ls or otherwise
let g:airline#extensions#tabline#fnamemod = ':t:r'
" w0rp/ALE configuration triggers
" Set this. Airline will handle the rest.
let g:airline#extensions#ale#enabled = 1

