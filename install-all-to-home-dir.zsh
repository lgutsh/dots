#!/usr/bin/env zsh

SOURCE=$PWD
TARGET=$HOME

for i in `ls` ; do ;
  echo "Installing from "$SOURCE"/"$i" to "$TARGET ;
  stow -d $SOURCE -t $TARGET $i --adopt ; done ;
