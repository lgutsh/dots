#!/usr/bin/env zsh
# Start x on first login
if [[ `ps aux | grep xinit | wc -l` -lt 1 ]]
then
	exec startx
fi
