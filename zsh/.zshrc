# Set up the prompt
autoload -Uz promptinit
promptinit
prompt adam1

setopt histignorealldups sharehistory

# Keep 1000 lines of history within the shell and save it to ~/.zsh_history:
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.zsh_history

# Use modern completion system
autoload -Uz compinit zrecompile
compinit
# completion from both ends
setopt completeinword

# superglobs
setopt extendedglob
unsetopt caseglob

# .. instead of cd .. and so on...
setopt auto_cd

# setting compat with bash for complete and stderr redirection
setopt multios

# display cpu stats for commands taking more than ten secs
REPORTTIME=10

# This allows bash completion to run inside of zsh!!!
autoload bashcompinit
bashcompinit

# Fixing envsetup script
unsetopt NOMATCH

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

# Configure zline
#source ~/.zsh/includes/.zsh-zline

# Add extra paths
source ~/.zsh/includes/.zsh-paths

# Set up the prompt line
#source ~/.zsh/includes/.zsh-promptline

# Load key bindings specific to zsh
source ~/.zsh/includes/.zsh-keybindings

# Load ripgrep settings
source ~/.zsh/includes/.zsh-ripgrep

# Load rust settings
source ~/.zsh/includes/.zsh-rust

# Load less settings
source ~/.zsh/includes/.zsh-less

# Load antigen plugins
source ~/.zsh/includes/.zsh-antigen

# Load default aliases
source ~/.zsh/includes/.zsh-aliases

# Load fzf configuration
# Installed from ~/scripts/external/fzf (submodule)
# After running the install script from above, by default this path is used:
source ~/.fzf.zsh

# TODO XXX What are these two?:
export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"
# source $HOME/.config/broot/launcher/bash/br

